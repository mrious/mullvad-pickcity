import unittest
import requests
from city import City, cities as local_cities

# Api Docs: https://api.mullvad.net/app/documentation/
api_url = "https://api.mullvad.net/app/v1/relays"


def compare_local_and_api_locations(
    api_cities: list[City],
) -> tuple[set[City], set[City]]:
    api_cities_set = {city for city in api_cities}
    local_cities_set = {city for city in local_cities}

    return (
        api_cities_set - local_cities_set,
        local_cities_set - api_cities_set,
    )


class TestCity(unittest.TestCase):
    def test_has_same_cities(self):
        api_request = requests.get(api_url, timeout=10)
        self.assertEqual(api_request.status_code, 200)
        api_response = api_request.json()
        api_locations = api_response["locations"]

        api_cities = [
            City(
                name=", ".join((meta["country"], meta["city"])),
                cc=location.split("-")[0],
                ccc=location.split("-")[1],
            )
            for location, meta in api_locations.items()
        ]

        new_cities, offline_cities = compare_local_and_api_locations(api_cities)
        new_cities_msg = "\nNew Cities detected:\n{0}".format(
            "\n".join([str(new_city) for new_city in new_cities])
        )
        offline_cities_msg = "\nOffline Cities detected:\n{0}".format(
            "\n".join([str(offline_city) for offline_city in offline_cities])
        )
        self.assertEqual(len(new_cities), 0, msg=new_cities_msg)
        self.assertEqual(len(offline_cities), 0, msg=offline_cities_msg)


if __name__ == "__main__":
    unittest.main()

# Mullvad PickCity

Picks a random Mullvad server location

## Usage

```
pickcity
```
Prints a random server location

```
pickcity -c
```
Prints a random server location and tries to connect to it using the Mullvad cli tool
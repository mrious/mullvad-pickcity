#!/usr/bin/python3
# Pick a Mullvad server location.
from argparse import ArgumentParser, RawTextHelpFormatter
from city import cities, eu, tpcc
from random import choice
from subprocess import run

parser = ArgumentParser(
    description="Pick a Mullvad server location.", formatter_class=RawTextHelpFormatter
)

parser.add_argument(
    "--cc", help="Limit results to one country (by ISO 3166-1 alpha-2 code)"
)
parser.add_argument(
    "--noeu",
    "--nogdpr",
    action="store_true",
    help="Only select from locations that are not subject to GDPR or similar laws",
)
parser.add_argument(
    "--tp",
    "--twitchpredict",
    action="store_true",
    help="""Only select from locations that are allowed to participate in Twitch's
Prediction program
See: https://help.twitch.tv/s/article/channel-points-predictions#viewerfaq

""",
)
parser.add_argument(
    "-c",
    "--connect",
    action="store_true",
    help="Try to connect to a server using the Mullvad cli tool",
)


args = parser.parse_args()

if args.noeu:
    cities = [city for city in cities if city.cc not in eu]

if args.tp:
    cities = [city for city in cities if city.cc not in tpcc and city.ccc != "mtr"]

if args.cc:
    cities = [city for city in cities if city.cc == args.cc]


if len(cities) == 0:
    print("error: no cities match specified conditions")
    exit(1)

city = choice(cities)
print(city)

if args.connect:
    run(["mullvad", "relay", "set", "location", city.cc, city.ccc])
    run(["mullvad", "connect"])
    run(["mullvad", "relay", "set", "tunnel-protocol", "any"])
